package com.example.acmesa

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.acmesa.data.model.informacion
import com.example.acmesa.databinding.CardUsuarioBinding

class AdapterDatos (private var data:List<informacion>) : RecyclerView.Adapter<AdapterDatos.ViewHolder>(){

    private lateinit var mContext: Context

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val binding = CardUsuarioBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext = parent.context
        val view = LayoutInflater.from(mContext).inflate(R.layout.card_usuario,parent,false)
        return ViewHolder(view)
    }
    override fun getItemCount(): Int = data.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val datos=data.get(position)
        with(holder){
            binding.nombres.text=datos.destino
            binding.apellidos.text=datos.destino


        }


    }
    fun setData(data: List<informacion>) {
        this.data = data
        notifyDataSetChanged()
    }
}