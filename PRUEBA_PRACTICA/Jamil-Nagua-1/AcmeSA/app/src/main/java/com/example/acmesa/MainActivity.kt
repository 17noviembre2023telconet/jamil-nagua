package com.example.acmesa

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.acmesa.data.model.DataSource
import com.example.acmesa.data.model.informacion
import com.example.acmesa.databinding.ActivityMainBinding
import com.example.acmesa.domin.RepoImpl
import com.example.acmesa.vo.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MainActivity : AppCompatActivity() {
    private lateinit var mBinding : ActivityMainBinding
    private val repo= RepoImpl(DataSource())
    private lateinit var dataAdapter: AdapterDatos
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        setContentView(R.layout.activity_main)
        startRecyclerView()

                GlobalScope.launch(Dispatchers.Main) {
                    var dato= withContext(Dispatchers.IO){

                        repo.getInformacion()
                    }
                    when(dato){
                        is Resource.Success -> {
                            Log.d("jamil",""+dato.data.results)
                            var lista = ArrayList<informacion>()
                            dato.data.results.forEach {
                                lista.add(
                                    informacion(
                                        it.email,
                                        it.location.city
                                    )

                                )
                            }
                            dataAdapter.setData(lista)

                        }
                        is Resource.Loading -> {
                            Log.d("jamil","esperando")

                        }
                        is Resource.Failure -> {
                            Log.d("jamil","falla")

                        }
                    }

                }

    }

    private fun startRecyclerView() {

        dataAdapter = AdapterDatos(mutableListOf())
        linearLayoutManager = LinearLayoutManager(this)
        mBinding.recylerServices.layoutManager=linearLayoutManager

        mBinding.recylerServices.adapter=dataAdapter



    }


}