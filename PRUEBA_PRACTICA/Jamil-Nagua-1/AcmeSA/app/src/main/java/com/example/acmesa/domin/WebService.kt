package com.example.acmesa.domin

import com.example.acmesa.data.model.DataPersona
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface WebService {

    @GET("/api/")
    suspend fun getUserInformation(@Query("results")results:String) : DataPersona
}