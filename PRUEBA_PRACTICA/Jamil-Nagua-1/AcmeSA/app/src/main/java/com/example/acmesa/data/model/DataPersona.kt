package com.example.acmesa.data.model

import com.google.gson.annotations.SerializedName

data class DataPersona(
    @SerializedName("results")
    val results:List<Result>,

    )
