package com.example.acmesa.domin

import com.example.acmesa.data.model.DataPersona
import com.example.acmesa.vo.Resource

interface DataSourceRepo {
    suspend fun getInformacion(): Resource<DataPersona>
}