package com.example.acmesa.data.model

import com.google.gson.annotations.SerializedName

data class Result(
    @SerializedName("name")
    val name:Name,
    @SerializedName("email")
    val email:String,
    @SerializedName("login")
    val login:Login,
    @SerializedName("location")
    val location:Location,


    )


data class Name(
    @SerializedName("first")
    val first:String,
    @SerializedName("last")
    val last:String,

    )


data class Login(
    @SerializedName("username")
    val username:String,
)
data class Location(
    @SerializedName("city")
    val city:String,
    @SerializedName("country")
    val country:String,
    @SerializedName("street")
    val street:Street,
    @SerializedName("coordinates")
    val coordinates:Coordinates,


    )
data class Street(
    @SerializedName("name")
    val name:String,
)
data class Coordinates(
    @SerializedName("latitude")
    val latitude:String,
    @SerializedName("longitude")
    val longitude:String,
)

