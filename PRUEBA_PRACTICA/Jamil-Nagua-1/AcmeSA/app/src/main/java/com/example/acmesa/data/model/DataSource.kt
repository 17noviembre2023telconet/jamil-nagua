package com.example.acmesa.data.model

import com.example.acmesa.domin.DataSourceRepo
import com.example.acmesa.vo.Resource
import com.example.acmesa.vo.RetrofitClient

class DataSource(): DataSourceRepo {
    var retrofi= RetrofitClient()

    override suspend fun getInformacion(): Resource<DataPersona> {
        return  Resource.Success(retrofi.webservice.getUserInformation("10"))
    }


}