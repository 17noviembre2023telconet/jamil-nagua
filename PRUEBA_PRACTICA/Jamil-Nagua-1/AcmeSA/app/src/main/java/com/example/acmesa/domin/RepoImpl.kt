package com.example.acmesa.domin

import com.example.acmesa.data.model.DataPersona
import com.example.acmesa.vo.Resource

class RepoImpl(private val dataSource: DataSourceRepo) :Repo {
    override suspend fun getInformacion(): Resource<DataPersona> {
        return dataSource.getInformacion()
    }


}